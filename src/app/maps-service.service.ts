import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class MapsServiceService {

  url="http://localhost:3000/api"
  constructor(private http:HttpClient) { }

  getCountryDetails(){
    return this.http.get(this.url+"/country");
  }
}
