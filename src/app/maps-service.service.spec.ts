import { TestBed } from '@angular/core/testing';

import { MapsServiceService } from './maps-service.service';

describe('MapsServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MapsServiceService = TestBed.get(MapsServiceService);
    expect(service).toBeTruthy();
  });
});
